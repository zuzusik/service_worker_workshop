angular.module('truthNews', ['ngResource', 'ngRoute']);

navigator.serviceWorker.getRegistration().then(function(registration) {
  if (!registration) {
    navigator.serviceWorker.register('/service-worker.js').then(function (registration) {
      console.log('SW registration successful');
    }).catch(function (err) {
      console.log('SW Registration failed: ', err);
    });
  }
});
