angular.module('truthNews')
  .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });

    $routeProvider.when('/', {
      templateUrl: 'views/news-list.view.html',
      controller: 'newsListCtrl'
    });

    $routeProvider.when('/news/:newsItemId', {
      templateUrl: 'views/news-item.view.html',
      controller: 'newsItemCtrl'
    });
  }]);
