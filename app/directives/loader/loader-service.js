angular.module('truthNews').service('loaderSrv', [function () {
  var loadingState = false;

  return {
    getLoadingState: function () { return loadingState; },
    show: function () { loadingState = true; },
    hide: function () { loadingState = false; }
  }
}]);
