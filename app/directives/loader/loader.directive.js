angular.module('truthNews').directive('loader', ['loaderSrv', function (loaderSrv) {
  return {
    scope: {},
    templateUrl: 'directives/loader/loader.template.html',
    link: function ($scope, element) {
      element.addClass('hide');
      $scope.$watch(
        function () { return loaderSrv.getLoadingState(); },
        function (loadingState) { element.toggleClass('hide', !loadingState); }
      );
    }
  }
}]);
