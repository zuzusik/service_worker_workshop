angular.module('truthNews').directive('updateNotifier', function () {
  return {
    scope: {},
    templateUrl: 'directives/update-notifier/update-notifier.template.html',
    controller: ['$scope', '$window', function ($scope, $window) {
      $window.navigator.serviceWorker.addEventListener('message', function(event) {
        if (event.data === 'newHotUpdate') {
          $scope.newHotUpdateAvailable = true;
          $scope.$digest();
        } else if (event.data === 'newColdUpdate') {
          $scope.newColdUpdateAvailable = true;
          $scope.$digest();
        }
      });

      $window.navigator.serviceWorker.getRegistration().then(function (registration) {
        if (registration && registration.waiting && registration.waiting.state === 'installed') {
          $scope.newColdUpdateAvailable = true;
        }
      });
    }]
  }
});
