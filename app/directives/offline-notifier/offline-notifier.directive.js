angular.module('truthNews').directive('offlineNotifier', function () {
  return {
    scope: {
      cacheTimestamp: '@'
    },
    templateUrl: 'directives/offline-notifier/offline-notifier.template.html'
  }
});
