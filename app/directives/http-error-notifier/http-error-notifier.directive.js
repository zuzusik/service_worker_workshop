angular.module('truthNews').directive('httpErrorNotifier', function () {
  return {
    scope: {
      error: '='
    },
    templateUrl: 'directives/http-error-notifier/http-error-notifier.template.html',
    controller: ['$scope', function ($scope) {
      $scope.getErrorMessage = function (error) {
        return [
          error.status || '',
          error.status
            ? error.statusText || 'Data loading failed.'
            : 'This information is not accessible in offline mode.'
        ].join(' ');
      };
    }]
  }
});
