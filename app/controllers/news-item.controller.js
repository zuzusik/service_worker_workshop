angular.module('truthNews').controller('newsItemCtrl', [
  '$scope', '$routeParams', 'newsResourceSrv', 'loaderSrv',
  function ($scope, $routeParams, newsResourceSrv, loaderSrv) {
    loaderSrv.show();

    newsResourceSrv.get({newsItemId: $routeParams.newsItemId}).$promise.then(function (response) {
      response.data.paragraphs = response.data.text.split('\n \r');
      $scope.newsItem = response.data;
      $scope.cacheTimestamp = response.headers('x-cache-timestamp');
    }).catch(function (error) {
      $scope.loadingError = error;
    }).finally(function () {
      loaderSrv.hide();
    });
  }
]);
