angular.module('truthNews').controller('newsListCtrl', [
  '$scope', 'newsResourceSrv', 'loaderSrv',
  function ($scope, newsResourceSrv, loaderSrv) {
    var newsOffset = 0,
        newsPageSize = 10;

    $scope.newsList = [];
    $scope.isNewsLoading = loaderSrv.getLoadingState;

    $scope.loadNextNewsPage = function () {
      loaderSrv.show();
      delete $scope.loadingError;
      newsResourceSrv.query({offset: newsOffset, size: newsPageSize}).$promise.then(function (response) {
        $scope.newsList =  $scope.newsList.concat(response.data);
        $scope.cacheTimestamp = response.headers('x-cache-timestamp');
        newsOffset += newsPageSize;
      }).catch(function (error) {
        $scope.loadingError = error;
      }).finally(function () {
        loaderSrv.hide();
      });
    };

    $scope.loadNextNewsPage();
  }
]);
