var staticVersion = 1,
    isHotUpdate = true;

if (isHotUpdate) {
  self.skipWaiting();
}

self.addEventListener('install', function(event) {
  var urlsToCache = [
    '/',
    '/app.css',
    '/app.js',
    '/vendor.js'
  ];

  event.waitUntil(
    caches.open('static-cache-v' + staticVersion).then(function(cache) {
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener('install', function (event) {
  if (!isHotUpdate) {
    sendToClients('newColdUpdate');
  }
});

self.addEventListener('activate', function(event) {
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.map(function(cacheName) {
          if (cacheName.startsWith('static-cache-v') && !cacheName.endsWith('-v' + staticVersion)) {
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});

self.addEventListener('activate', function() {
  if (isHotUpdate) {
    sendToClients('newHotUpdate');
  }
});

self.addEventListener('fetch', function(event) {
  if (new URL(event.request.url).pathname.startsWith('/api/')) {
    handleAPIRequests(event);
  } else {
    handleStaticRequests(event);
  }
});

function handleAPIRequests (event) {
  event.respondWith(
    fetch(event.request).then(function (response) {
      var url = new URL(event.request.url);

      if (response.status === 200 && url.pathname !== '/api/news' || url.search === '?offset=0&size=10') {
        caches.open('api-cache-v1').then(function (cache) {
          response.clone().text().then(function (responseText) {
            var responseToCache = new Response(responseText, {headers: response.headers});

            responseToCache.headers.set('x-cache-timestamp', new Date().getTime());
            cache.put(event.request, responseToCache).then(function () {
              rotateAPICache(cache, 5);
            });
          });
        });
      }
      return response.clone();
    }).catch(function (error) {
      return caches.open('api-cache-v1').then(function (cache) {
        return cache.match(event.request).then(function (cachedResponse) {
          if (cachedResponse) {
            return cachedResponse;
          } else {
            return error;
          }
        });
      });
    })
  );
}

function handleStaticRequests (event) {
  var requestToMatch = new URL(event.request.url).pathname.match(/^\/news\/\d*$/)
    ? new Request('/')
    : event.request;

  event.respondWith(
    caches.open('static-cache-v' + staticVersion).then(function (cache) {
      return cache.match(requestToMatch).then(function (cachedResponse) {
        if (cachedResponse) {
          return cachedResponse;
        } else {
          return fetch(requestToMatch);
        }
      });
    })
  );
}

function rotateAPICache (cache, size) {
  return cache.keys().then(function (keys) {
    if (keys.length > size) {
      for (var i = keys.length; i === 0; i--) {
        var url = new URL(keys[i].url);

        if (url.pathname + url.search !== '/api/news?offset=0&size=10') {
          cache.delete(keys[i]);
          break;
        }
      }
    }
  });
}

function sendToClients (message) {
  return self.clients.matchAll({includeUncontrolled: true}).then(function (clients) {
    for (var i in clients) {
      clients[i].postMessage(message);
    }
  });
}
