module.exports = function (app) {
  var faker = require('faker');

  function daysToTime (days) { return days * 24 * 60 * 60 * 1000; }

  app.get('/api/news', function (req, res) {
    var news = [],
        size = parseInt(req.query.size || 10),
        daysFrom = parseInt(req.query.offset || 0),
        daysTo = daysFrom + size;

    for (var i = 1; i <= size; i++) {
      news.push({
        id: faker.random.number(1000000000),
        title: faker.lorem.sentence(),
        subtitle: faker.lorem.sentences(),
        date: faker.date.between(
          new Date(new Date() - daysToTime(daysFrom)),
          new Date(new Date() - daysToTime(daysTo))
        ),
        picture: faker.image.imageUrl(160, 120) + '/?rand=' + Math.random()
      });
    }

    news.sort(function (a, b) {
      if (a.date > b.date) {
        return -1;
      } else if (a.date < b.date) {
        return 1;
      } else {
        return 0;
      }
    });

    res.json(news);
  });

  app.get('/api/news/:newsItemId', function (req, res) {
    var newsItem = {
        id: req.params.newsItemId,
        title: faker.lorem.sentence(),
        text: faker.lorem.paragraphs(5),
        date: faker.date.past(),
        picture: faker.image.imageUrl(320, 240) + '/?rand=' + Math.random()
      };

    res.json(newsItem);
  });
};
